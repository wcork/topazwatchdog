import os
import queue
import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from watchdog.events import FileSystemEvent

TASK_QUEUE = queue.Queue()


def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path:  # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts


class TaskItem(object):
    def __init__(self, project_name, project_file, project_file_subpath):
        self.project_name = project_name
        self.project_file = project_file
        self.project_file_subpath = project_file_subpath

    def __str__(self):
        return f'{self.project_name}: {{{self.project_file}, {self.project_file_subpath}}}'


class TopazWatchdogEventHandler(PatternMatchingEventHandler):
    """
    Matches given patterns with file paths associated with occurring events.
    <watch-folder>/INCOMING/<project-name>/
    """

    def __init__(self, watchfolder):
        super().__init__(patterns=[os.path.join(watchfolder, "INCOMING", "**", "*.mp4"), os.path.join(watchfolder, "INCOMING", "**", "*.mov")],
                         ignore_patterns=[os.path.join(watchfolder, "COMPLETE", "**")], case_sensitive=False)
        self._watchfolder = watchfolder

    @property
    def watchfolder(self):
        """
        (Read-only)
        """
        return self._watchfolder

    def on_created(self, event: FileSystemEvent):
        super().on_created(event)
        what = 'directory' if event.is_directory else 'file'
        logging.root.info("Created %s: %s", what, event.src_path)
        path_split = splitall(event.src_path)
        logging.root.info("split path: %s", path_split)
        incoming_idx = path_split.index("INCOMING")
        if len(path_split) - incoming_idx - 1 < 2:
            # Ensure we are only handling files that are at least within a project
            return
        logging.root.info("Queueing task: (%s, %s)", path_split[incoming_idx+1], event.src_path)
        TASK_QUEUE.put(TaskItem(project_name=path_split[incoming_idx+1], project_file=event.src_path, project_file_subpath=os.path.join(*path_split[incoming_idx+2:])))


def process_queue(project_path):
    if TASK_QUEUE.qsize() > 0:
        taskItem = TASK_QUEUE.get()
        logging.root.info("Executing task: %s", taskItem)

        # <watch-folder>/COMPLETE/<project-name>/
        complete_project_root_path = os.path.join(project_path, "COMPLETE", taskItem.project_name, os.path.split(taskItem.project_file_subpath)[0])
        os.makedirs(complete_project_root_path, exist_ok=True)
        logging.root.info("Task Complete Saving: %s", os.path.join(complete_project_root_path, taskItem.project_file_subpath))
        TASK_QUEUE.task_done()
    else:
        return


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = TopazWatchdogEventHandler(path)
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
            process_queue(path)
    finally:
        observer.stop()
        observer.join()
