# TopazWatchdog Service
A simple Python service that watches a folder and queues Topaz Video AI upscaler tasks.

## Folder structure

The `watch-folder` expects 2 sub-directories called `INCOMING` and `COMPLETE`. Within these directories, a specified 
`project-name` folder is expected that contains files to be processed in Topaz.

All output processing jobs will be stored in the `COMPLETE` folder with the same `project-name` and file name.

```
<watch-folder>/INCOMING/<project-name>/
<watch-folder>/COMPLETE/<project-name>/
```